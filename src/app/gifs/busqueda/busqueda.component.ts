import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GifsService } from '../services/gifs.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css'],
})
export class BusquedaComponent implements OnInit {
  @ViewChild('txtbuscar') txtbuscar!: ElementRef<HTMLInputElement>;

  constructor(private gifsService:GifsService) {}

  ngOnInit(): void {}

  buscar() {
    const valor = this.txtbuscar.nativeElement.value;
    if (valor===''){
     return
    }
    //console.log(valor);
    this.gifsService.buscarGifs(valor)

    this.txtbuscar.nativeElement.value = '';
  }
}
